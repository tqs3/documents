# Farmacy Web App and Delivery Guys

## Project Abstract

O nosso sistema contém uma API e uma Web App, Delivery Guys e Farmacy Web App respetivamente, onde a Farmacy Web App simula o comportamento de negócio de uma farmacia e a API possibilita o serviço de entregas multiplataforma aos utilizadores.

## Project Team

**Product Owner**: [Nuno Matamba - 78444](https://gitlab.com/nunomatamba)

**Team Manager**: [Eleandro Laureano - 83069](https://gitlab.com/EleandroGG)

**DevOps Master**: [Miguel Marques - 100850](https://gitlab.com/mikemike198)

**QA Engineer**: [Frederico Avó - 79900](https://gitlab.com/freddavo)

## Setup

**Databases:**<br />
Como as bases de dados das nossas plataformas estão na VM, para puder acede-las tem de se ter o VPN da UA ligado.<br />
**Swagger:**<br />
O Swagger esta a correr com o projeto Delivery Guys localmente, sendo o link http://localhost:8081/delivery/swagger-ui.html#/<br />
**SonarQube :**<br />
O analise do SonarQube do lado do sistema de entregas esta correr localmente.<br />
Para testar terá de se ter o SonarQube a correr no computador.<br />

## Links
- [Pivotal Tracker](https://www.pivotaltracker.com/n/projects/2500853)
- [Farmacy Web App](https://gitlab.com/tqs3/farmacywebapp)
- [Delivery Guys](https://gitlab.com/tqs3/entregasapi)
- [Repositorio Antigo](https://gitlab.com/tqs2/tqs-projeto)
- [Sonarcloud - entregasapi](https://sonarcloud.io/dashboard?id=tqs3_entregasapi)
- [Sonarcloud - farmacyWebApp](https://sonarcloud.io/dashboard?id=farmacyWebApp)

